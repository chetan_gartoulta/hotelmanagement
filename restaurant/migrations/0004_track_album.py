# Generated by Django 4.0.5 on 2022-07-01 13:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant', '0003_alter_customer_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Track',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.CharField(max_length=255)),
                ('title', models.CharField(max_length=255)),
                ('duration', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('album_name', models.CharField(max_length=255)),
                ('artist', models.CharField(max_length=255)),
                ('tracks', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restaurant.track')),
            ],
        ),
    ]
