from datetime import date, datetime
from distutils.command.upload import upload
from email.mime import image
from email.policy import default
from tabnanny import check
from tkinter import Canvas
from django.db import models
import string
import random
from django.contrib.auth.models import User
from phone_field import PhoneField

import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image,ImageDraw

def random_string_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))





class Customer(models.Model):
    user = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255,null=True)
    # user = models.OneToOneField(User,on_delete=models.CASCADE,default=True)
    age = models.CharField(max_length=255,blank=True)
    location = models.CharField(max_length=255,blank=True)
    phone_number = models.IntegerField(blank=True, help_text='Contact phone number')
    nationality = models.CharField(max_length=155,blank=True)
    country = models.CharField(max_length=255,blank=True)
    check_in = models.DateTimeField(auto_now=False,null=True)
    check_out = models.DateTimeField(auto_now=False,null=True)
    qr_code = models.ImageField(upload_to='customer_qr',blank=True)
    # profile_image  = models.ImageField(upload_to = "user_profile/",blank=True)

    @property
    def totaldays_stayed(self):
        cdt = datetime.today().timestamp()
        print(cdt)
        cin = datetime.strptime(str(self.check_in),'%Y-%m-%d %H:%M:%S').timestamp()
        cout = datetime.strptime(str(self.check_out),'%Y-%m-%d %H:%M:%S').timestamp()
        time_diffrence = cout-cin
        time_spend = cdt-cin
        remaing_days = (time_spend/time_diffrence)*100
       
        
        return int(remaing_days)
    
    def format_date(obj):
        return obj.check_in.strftime('%d %b %Y %H:%M')
    format_date.short_description = 'Modified'


# creting Qr code ......
    def save(self,*args, **kwargs):
        qrcode_image = qrcode.make(self.user)
        canvas = Image.new("RGB",(300,300),"white")
        draw = ImageDraw.Draw(canvas)
        canvas.paste(qrcode_image)
        fname=f'qr_code-{self.user}.png'
        buffer = BytesIO()
        canvas.save(buffer,"PNG")
        self.qr_code.save(fname,File(buffer),save=False)
        canvas.close()
        super().save(*args,**kwargs)


    def __str__(self):
        return self.user


class Room(models.Model):
    room_status = (
        ("available", "available"),
        ("booked", "booked"),
        ("cleaning", "cleaning"),)
    room_name = models.CharField(max_length=255, default=True)
    # onetoone relationship with User
    room_number = models.CharField(max_length=255, unique=True)
    # choice filed will be include
    room_type = models.CharField(max_length=255)
    price_pernight = models.CharField(max_length=255)
    max_person = models.IntegerField()
    room_description = models.CharField(max_length=500,)
    booking_timeframe = models.CharField(max_length=500)
    image = models.ImageField(upload_to="room/room_image",blank=True)

    status = models.CharField(
        max_length=255, choices=room_status, default="av")




    def __str__(self):
        return f"{self.room_name}"


class Booked(models.Model):
    # id = models.IntegerField(primary_key=True)
    customer_details = models.ForeignKey(
        Customer, on_delete=models.CASCADE, null=True)
    room_id = models.ForeignKey(Room, on_delete=models.CASCADE,blank=False)

    class Meta:
        ordering =["-customer_details"]
        
    @property
    def total_room(self):
        total_room = self.room_id.objects.all()
        return total_room

    def __str__(self):
        return f'{self.customer_details}'

    


class Category(models.Model):
    category_name = models.CharField(max_length=255)

    def __str__(self):
        return self.category_name

 


class Kitchen_items(models.Model):
    product_name = models.CharField(max_length=255)
    price = models.FloatField(max_length=1000)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE)
    qty = models.FloatField(max_length=255)

    def __str__(self):
        return self.product_name


class Bar_items(models.Model):
    product_name = models.CharField(max_length=255)
    price = models.FloatField(max_length=1000)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE)
    qty = models.FloatField(max_length=255)

    def __str__(self):
        return self.product_name


class Order(models.Model):
    PAYMENTTYPE = (('PAID', 'PAID'), ('UNPAID', 'UNPAID'))
    PRODUCTDELIVARY = (('NEW', 'NEW'), ('APPROVED', 'APPROVED'),('CANCLE','CANCLE'),('DISPATCHED','DISPATCHED'),('RECIVED','RECIVED'),('Delivered','Delivered'))
    order_id = models.CharField(max_length=255,default = random_string_generator,unique=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, blank=True)
    kitchen_item = models.ManyToManyField(Kitchen_items,blank=True)
    bar_items = models.ManyToManyField(Bar_items,blank=True)
    qty = models.FloatField(max_length=255, null=True)
    total = models.CharField(max_length=255, null=True)
    order_status = models.CharField(max_length=255,choices=PRODUCTDELIVARY,null=True,default="NEW")
    payment_type = models.CharField(max_length=255,choices=PAYMENTTYPE,null=True,default="UNPAID")
    order_date= models.DateTimeField(auto_created=True,auto_now=True,null=True)

    def __str__(self):
        return self.customer.user




