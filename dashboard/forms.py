from dataclasses import fields
from pyexpat import model
from django import forms

from restaurant.models import Bar_items, Customer, Kitchen_items, Order




class OrderCreationForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'
        exclude =["id"]
        
        widgets ={
            "order_id" : forms.TextInput(attrs={"class":"form-control"}),
            "customer" : forms.Select(attrs={"class":"form-control"}),
            "kitchen_item" : forms.SelectMultiple(attrs={"class":"form-control"}),
            "bar_items" : forms.SelectMultiple(attrs={"class":"form-control"}),
            "qty" : forms.NumberInput(attrs={"class":"form-control"}),
            "total" : forms.NumberInput(attrs={"class":"form-control"}),
            "order_status" : forms.Select(attrs={"class":"form-control"}),
            "payment_type" : forms.Select(attrs={"class":"form-control"}),
        }



class CustomerCretionForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = "__all__"
        exclude = ["id","qr_code"]
        widgets ={
        "user" : forms.TextInput(attrs={"class":"form-control"}),
        "email" : forms.TextInput(attrs={"class":"form-control"}),
        "age" : forms.TextInput(attrs={"class":"form-control"}),
        "location" : forms.TextInput(attrs={"class":"form-control"}),
        "phone_number" : forms.TextInput(attrs={"class":"form-control"}),
        "nationality" : forms.TextInput(attrs={"class":"form-control"}),
        "country" : forms.TextInput(attrs={"class":"form-control"}),
        "check_in" : forms.DateInput(attrs={"class":"form-control"}),
        "check_out" : forms.DateInput(attrs={"class":"form-control"}),
        }




class KitchenItemCreationForm(forms.ModelForm):
    class Meta:
        model = Kitchen_items
        fields = "__all__"
        exclude = ["id",]

        widgets ={
        "product_name" : forms.TextInput(attrs={"class":"form-control"}),
        "price" : forms.TextInput(attrs={"class":"form-control"}),
        "qty" : forms.TextInput(attrs={"class":"form-control"}),
        "categories" : forms.Select(attrs={"class":"form-control"}),
        }

class BarItemCreationForm(forms.ModelForm):
    class Meta:
        model = Bar_items
        fields = "__all__"
        exclude = ["id",]
        widgets ={
        "product_name" : forms.TextInput(attrs={"class":"form-control"}),
        "price" : forms.TextInput(attrs={"class":"form-control"}),
        "qty" : forms.TextInput(attrs={"class":"form-control"}),
        "categories" : forms.Select(attrs={"class":"form-control"}),
        }