$(document).ready(() => {
  $("#tbody", "tbody", "tr").click(function () {
    alert($(this.text()))
  })


  $("#customers").click((e) => {

    e.preventDefault()
    $("#loading_page").load("/reservation/")

    $('table').SetEditable({
      columnsEd: "0,1,",
      onEdit: function () { },
    });

  })
  $("#create_user").click((e) => {
    e.preventDefault()
    let username = $("#val-username").val()
    let email = $("#val-email").val()
    let age = $("#val-age").val()
    let loacation = $("#val-location").val()
    let phonenumber = $("#val-phonenumber").val()
    let nationality = $("#val-nationality").val()
    let country = $("#val-country").val()
    let room_id = $("#val-room").val()
    let csr = $("input[name=csrfmiddlewaretoken]").val()


    mydata = { user: username, email: email, age: age, loacation: loacation, phonenumber: phonenumber, nationality: nationality, room_id: room_id, csrfmiddlewaretoken: csr, country: country }

    $.ajax({
      url: "/reservation/",
      data: mydata,
      method: "POST",
      datatype: "json",
      success: (data) => {
        if (data.data == "created") {
          alert("Reservation is sucessfull")

          var output = ''
          fetch("/Reservation_booking/")
            .then((resp) => resp.json()).then(function (data) {
              list = data
              for (var i in list) {
                // console.log(list[i]["customer_details"]["check_in"])
                output += `<tr><td>${list[i]["customer_details"]["user"]}</td>
                <td>
                  <div class="progress" style="background: rgba(127, 99, 244, .1)">
                    <div class="progress-bar bg-primary" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                       </div>
                        </div>
                </td>
      <td>${list[i]["customer_details"]["check_in"]}</td>
      <td>${list[i]["customer_details"]["check_out"]}</td>
      <td>${list[i]["room_id"]["room_name"]}</td>
      <td ><span class ="badge badge-primary">${list[i]["room_id"]["room_number"]}</span></td>
      <td> <span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted"></i> </a> <a href="javascript:void()" data-toggle="tooltip" data-placement="top" title="Close"><i class="fa fa-close color-danger"></i></a></span>
 </td> </tr>`
              }
              $("tbody").html(output);
              $('form')[1].reset();

            })
        }
        if (data.data == "error") {
          alert("Form cant submitted")
        }
      }

    })




  })

  //  form validate:





});





  

$(document).ready(()=>{
  let csr = $("input[name=csrfmiddlewaretoken]").val()


  $('.editBtn').on('click',function(){
    //hide edit span
    $(this).closest("tr").find(".editSpan").hide();
    
    //show edit input
    $(this).closest("tr").find(".editInput").show();
    
    //hide edit button
    $(this).closest("tr").find(".editBtn").hide();
    
    //show edit button
    $(this).closest("tr").find(".saveBtn").show();
    
});

$('.saveBtn').on('click',function(){
    var trObj = $(this).closest("tr");
    console.log(trObj)
    var ID = $(this).closest("tr").attr('id');
    var inputData = $(this).closest("tr").find(".editInput").serialize();
    $.ajax({
        method:'PUT',
        url:"/customer_update_delete/"+ID+'/',
        dataType: "json",
        headers:{"X-CSRFToken": csr,},
        data:inputData,
        success:function(response){
          
            if(response.id == ID){
              
                trObj.find(".editSpan.user").text(response.user);
                
               
                trObj.find(".editInput.user").text(response.user);
             
                
                trObj.find(".editInput").hide();
                trObj.find(".saveBtn").hide();
                trObj.find(".editSpan").show();
                trObj.find(".editBtn").show();
            }else{
                alert(response.msg);
            }
        }
    });
});

$('.deleteBtn').on('click',function(){
    //hide delete button
    $(this).closest("tr").find(".deleteBtn").hide();
    
    //show confirm button
    $(this).closest("tr").find(".confirmBtn").show();
    
});

$('.confirmBtn').on('click',function(){
    var trObj = $(this).closest("tr");
    var ID = $(this).closest("tr").attr('id');
    $.ajax({
        type:'DELETE',
        url:"/customer_update_delete/"+ID+'/',
        headers:{"X-CSRFToken": csr,},
        dataType: "json",
        data:'action=delete&id='+ID,
        success:function(response){
            if(response == null){
                trObj.remove();
                console.log("data delete")
            }else{
                trObj.find(".confirmBtn").hide();
                trObj.find(".deleteBtn").show();
                alert("Cant allow");
            }
        }
    });
});


})

