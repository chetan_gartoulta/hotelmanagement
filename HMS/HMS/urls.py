

from django.contrib import admin
from django.urls import path, include,re_path
from schema_graph.views import Schema
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf import settings
from django.conf.urls.static import static


schema_view = get_schema_view(
   openapi.Info(
      title="Hotal Management System",
      default_version='v1',
      description="This is very First API enpoint for HMS",
      terms_of_service="https://www.google.com/policies/terms/",
      # contact=openapi.Contact(email="contact@snippets.local"),
      # license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("restaurant.urls")),
    path("schema/", Schema.as_view()),
    path('api-auth/', include('rest_framework.urls')),
    path("",include("dashboard.urls")),
    
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('api/api.json/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   #  path('', schema_view.without_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)