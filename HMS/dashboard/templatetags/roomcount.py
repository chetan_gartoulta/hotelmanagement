from tabnanny import check
from tkinter.font import ROMAN
from zoneinfo import available_timezones
from django import template

from restaurant.models import Customer, Room


register = template.Library()



@register.simple_tag()
def total_room():
    total_room = Room.objects.all().count()
    return total_room


@register.simple_tag()
def available_rooms():
    available_rooms = Room.objects.filter(status="available").count()
    return available_rooms



@register.simple_tag()
def booked_rooms():
    booked_rooms = Room.objects.filter(status="booked").count()
    return booked_rooms


@register.simple_tag()
def on_cleaning_rooms():
    on_cleaning_rooms = Room.objects.filter(status="cleaning").count()
    return on_cleaning_rooms


