
from django.forms import UUIDField
from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework import serializers,fields
from .models import(Customer, Order, Room, Booked,
                    Category, Kitchen_items, Bar_items)
from rest_framework.serializers import ValidationError


class CustomerSeriallizer(serializers.ModelSerializer):
    # check_in = fields.DateField(format=('%W'))
    # check_out = fields.DateField(format=('%Y-%m-%dT%H:%M'))

    class Meta:
        model = Customer
        fields = ("id","user","email","age","location","phone_number","nationality","country","check_in","check_out")


class RoomListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = "__all__"


class RoomBookedSerializer(WritableNestedModelSerializer):


 

    class Meta:
        model = Booked
        fields ="__all__"
        depth = 1



  

class CategoriesSeriallizer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class KitchenItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kitchen_items
        fields = "__all__"
        depth = 1


class BarItemsseriallizer(serializers.ModelSerializer):
    class Meta:
        model = Bar_items
        fields = "__all__"

        depth = 1


class OrderSerializer(serializers.ModelSerializer):
    customer = CustomerSeriallizer()
    kitchen_item = KitchenItemsSerializer(many=True)
    bar_items = BarItemsseriallizer(many=True)

    class Meta:
        model = Order
        fields = "__all__"
