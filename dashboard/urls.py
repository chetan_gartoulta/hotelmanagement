from django import views
from django.urls import path
from .views import Bar_item, Bar_items, Kitchen_item, Kitchen_item_update, Kitchen_items, bar_item_update, delete_bar_items, delete_kitchen_items, homepage,customer,booking,reservation,calender_fields,update_user_dates,order,customer_update,delete_customer,order_update,order_delete
urlpatterns = [
    path("index/",homepage,name="homepage"),
    path("booking/",booking,name="booking"),
    path("reservation/",reservation,name="reservation"),
    path("calender_fields/",calender_fields,name="calender_fields"),
    path("update_user_dates/",update_user_dates,name="update_user_dates"),
    path("order/",order,name="order"),
    path("order_update/<int:pk>/",order_update,name="order_update"),
    path("order_delete/<int:pk>/",order_delete,name="order_delete"),
    path("customer/",customer,name="customer"),
    path("customer_update/<int:pk>/",customer_update,name="customer_update"),
    path("delete_customer/<int:pk>/",delete_customer,name="delete_customer"),
    path("Kitchen_items/",Kitchen_item,name="Kitchen_items"),
    path("Kitchen_items_update/<int:pk>/",Kitchen_item_update,name="Kitchen_item_update"),
    path("delete_kitchen_items/<int:pk>/",delete_kitchen_items,name="delete_kitchen_items"),
    
    path("bar_items/",Bar_item,name="Bar_items"),
    path("bar_items_update/<int:pk>/",bar_item_update,name="bar_item_update"),
    path("bar_items_delete/<int:pk>/",delete_bar_items,name="bar_item_delete"),


]
