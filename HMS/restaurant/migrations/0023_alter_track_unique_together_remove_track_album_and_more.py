# Generated by Django 4.0.5 on 2022-07-16 06:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restaurant', '0022_album_track'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='track',
            unique_together=None,
        ),
        migrations.RemoveField(
            model_name='track',
            name='album',
        ),
        migrations.AddField(
            model_name='customer',
            name='email',
            field=models.EmailField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='status',
            field=models.CharField(choices=[('available', 'available'), ('booked', 'booked')], default='av', max_length=255),
        ),
        migrations.DeleteModel(
            name='Album',
        ),
        migrations.DeleteModel(
            name='Track',
        ),
    ]
