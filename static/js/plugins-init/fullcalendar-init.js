$(document).ready(function(){

  function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$('#external-events .external-event').each(function() {

  // store data so the calendar knows to render an event upon drop
  $(this).data('event', {
    title: $.trim($(this).text()), // use the element's text as the event title
    stick: true // maintain when user navigates (see docs on the renderEvent method)
  });

  // make the event draggable using jQuery UI
  $(this).draggable({
    zIndex: 999,
    revert: true,      // will cause the event to go back to its
    revertDuration: 0  //  original position after the drag
  });

});




   
  
    fetch("http://localhost:8000/calender_fields/ ")
    .then((resp) => resp.json()).then(function (data){
      console.log("this is data ",data)

//stating the calender ...
      $("#calendar").fullCalendar({
        height: 600,
        themeSystem: 'bootstrap4',
        header:{
            left:"next",
            center:"title",
            
        },
        eventLimit :true,
        
        events:data,
        selectable: true,
        selectHelper: true,
        editable: true,
        droppable: true,

        select: function(start, end, allDays) {
          $('#event-modal').modal('toggle');
        
          $("#b_res").click(function(){

            var title = $('#title').val();
            var start_date = moment(start).format('YYYY-MM-DD');
            var end_date = moment(end).format('YYYY-MM-DD');
            let username = $("#val-username").val()
            let email = $("#val-email").val()
            let age = $("#val-age").val()
            let loacation = $("#val-location").val()
            let phonenumber = $("#val-phonenumber").val()
            let nationality = $("#val-nationality").val()
            let country = $("#val-country").val()
            let room_id = $("#val-room").val()
            let csr = $("input[name=csrfmiddlewaretoken]").val()
            var mydata = { user: username, email: email, age: age, loacation: loacation, phonenumber: phonenumber, nationality: nationality, room_id: room_id, csrfmiddlewaretoken: csr, country: country,start_date:start_date,end_date:end_date }
            console.log(mydata)
          
            $.ajax({

              url:"http://localhost:8000/booking/",
              type:"POST",
              dataType:"application/json",
              data:mydata,
              headers: {"X-CSRFToken": getCookie("csrftoken")},
              success:function(data){
                
                $('#event-modal').modal('toggle');
                // console.log(data)
                  $("#myform").reset()
                  $('#event-modal').modal('toggle');
                $('#calendar').fullCalendar('renderEvent', {
                  'title': response.title,
                  'start' : response.start,
                  'end'  : response.end,
                  'color' : response.color,
                  'user':response.user.user
              });

              },
              error:function(data){
                if (data=="error"){
                alert("cant send data")
                }
              },

            })

          })
        
        },
        // this is for updateing the dates of events
    
        eventClick:function(calEvent, jsEvent, view){
          // console.log(calEvent)
          if (calEvent){
            console.log(calEvent)
              Swal.fire({
              title: 'Details',
              text: "Room " + calEvent["title"] + " is Booked by " + calEvent.user.user + " " + calEvent.start.format() + " to " + calEvent.end.format() ,
              // icon: 'warning',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              // cancelButtonColor: '#d33',
              confirmButtonText: 'Back to dash!'
            })
          }
          },
          // this is drop menu from the side list ,....
        drop: function(date) {
          alert("This is droppeed!!!!")
          // is the "remove after drop" checkbox checked?
          if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            $(this).remove();
          }
          alert("Dropped on " + date.format());
        },

        // this is for resizing the events
      eventResize: function(event, delta, revertFunc) {
        
          alert(event.title + " end is now " + event.end.format());
      
          if (!confirm("is this okay?")) {
            revertFunc();
          }
          else{
            let user = event.user.user
            let start_date = event.start.format()
            let end_date = event.end.format()
            let url = "http://localhost:8000/update_user_dates/"
            var  data = {user:user,start_date:start_date,end_date:end_date}
            var headers= {"X-CSRFToken": getCookie("csrftoken")}
            // console.log(user,start_date,end_date)

            ajax_post(url,data,headers)
          }
      
        },
        // this is for the events darg from one to another line or date change....
        eventDrop: function(event, delta, revertFunc) {

          alert(event.title + " was dropped on " + event.start.format());
      
          if (!confirm("Are you sure about this change?")) {
            revertFunc();
          }
          else{
            let user = event.user.user
            let start_date = event.start.format()
            let end_date = event.end.format()
            let url = "http://localhost:8000/update_user_dates/"
            var  data = {user:user,start_date:start_date,end_date:end_date}
            var headers= {"X-CSRFToken": getCookie("csrftoken")}
            // console.log(user,start_date,end_date)

            ajax_post(url,data,headers)
          }
      
        },
        
        
    });


    })
   


    function ajax_post(url, formdata){

      $.ajax({
        
        url:url,
        type:"POST",
        dataType:"application/json",
        data:formdata,
        headers: {"X-CSRFToken": getCookie("csrftoken")},
          
        success:function (data){
          console.log(data);
      },
    
    
    });
  }
    

    
    });