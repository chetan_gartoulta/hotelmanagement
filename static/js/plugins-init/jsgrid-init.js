$(document).ready(function() {




    var MyDateField = function(config) {
        jsGrid.Field.call(this, config);
    };
     
    MyDateField.prototype = new jsGrid.Field({
     
    
        sorter: function(date1, date2) {
            return new Date(date1) - new Date(date2);
        },
     
        itemTemplate: function(value) {
            return new Date(value).toDateString();
        },
     
        insertTemplate: function(value) {
            return this._insertPicker = $("<input>").datepicker({ defaultDate: new Date() });
        },
     
        editTemplate: function(value) {
            return this._editPicker = $("<input>").datepicker().datepicker("setDate", new Date(value));
        },
     
        insertValue: function() {
            return this._insertPicker.datepicker("getDate").toISOString();
        },
     
        editValue: function() {
            return this._editPicker.datepicker("getDate").toISOString();
        }
    });
     
    jsGrid.fields.date = MyDateField;


    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }








    $("#jsGrid-basic").jsGrid({
        height: "auto",
        width: "100%",
        filtering: true,
        editing: true,
        paging: true,
        sorting: true,
        paging: true,
        autoload: true,
        deleteConfirm: "Do you really want to delete the client?",
        pageSize: 15,
        pageButtonCount: 5,
        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
 
                $.ajax({
                    Headers:{"X-CSRFToken": getCookie("csrftoken")},
                    type:"GET" ,
                    url: "http://localhost:8000/Reservation_booking/",
                    dataType: "json",
                    data:filter
                }).done(function(result){
                    // console.log(result)
                    d.resolve($.map(result,function(item){
                        console.log(item)
                        return $.extend(item.fields, { Username : item ['customer_details']['user'] ,'Check in':item ['customer_details']['check_in']
                    
                        ,'Check out':item ['customer_details']['check_out'],
                        "Room Number":item["room_id"]["room_number"],
                        "Room Name":item["room_id"]["room_name"],
                        "id":item.id
                    
                    });
                    }))
                });
                return d.promise();
            },
            deleteItem: function(item) {

                return $.ajax({
                    type: "DELETE",
                    url: "http://localhost:8000/Reservation_booking_update_delete/" + item.id
                });
            }
           
        },
 
        fields: [
            { name: "Username", type: "text" ,width:100},
            { name: "Room Name", type: "text", width: 150 },
            
            {name:"Check in",type:"date"},
            {name:"Check out",type:"date" ,},
            {name:"Room Number",type:"number"},
            {type: "control", modeSwitchButton: false, editButton: false}
        ]
    });
   
});