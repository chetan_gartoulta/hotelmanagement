import random
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render,redirect
from dashboard.forms import CustomerCretionForm, OrderCreationForm,KitchenItemCreationForm,BarItemCreationForm
from restaurant.models import Bar_items, Booked, Customer, Kitchen_items, Order, Room
from http import HTTPStatus
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render,get_object_or_404


def homepage(request):
    return render(request, "index.html")


@csrf_exempt
def booking(request):
    cs = Booked.objects.all()
    av_room = Room.objects.filter(status="available")
    print(request.POST)
    if request.method == "POST":
        user = request.POST.get("user", "")
        email = request.POST.get("email", "")
        age = request.POST.get("age", "")
        location = request.POST.get("loacation", "")
        phone_number = request.POST.get("phonenumber", "")
        nationality = request.POST.get("nationality", "")
        country = request.POST.get("country", "")
        room_id = request.POST.get("room_id", "")
        start_date = request.POST.get("start_date", "")
        end_date = request.POST.get("end_date", "")
        print("this is message")

        print(user, email, age, location, phone_number,
              nationality, country, room_id)

        if user != '' and email != '' and age != '' and location != '' and phone_number != '' and nationality != '' and country != '' and room_id != '':

            obj = Customer.objects.create(user=user, email=email, age=age, location=location, phone_number=phone_number,
                                          nationality=nationality, country=country, check_in=start_date, check_out=end_date)
            obj.save()

            print("customer created!!!")
            # room_update=Room.objects.get(id=room_id)
            # room_update.status="booked"
            # room_update.save()

            customer_details = Customer.objects.all().last()
            room_id = Room.objects.get(id=room_id)

            obj = Booked.objects.create(
                customer_details=customer_details, room_id=room_id)

            obj.save()

            booking = list(Booked.objects.values())

            return JsonResponse({"data": "created", "booking": booking}, safe=False, status=HTTPStatus.CREATED)
        return JsonResponse({"data": "error"}, safe=False)

    context = {"customers": cs, "av_room": av_room, }
    return render(request, "forms/booking_form.html", context)


def reservation(request):
    av_room = Room.objects.filter(status="available")

    return render(request, "forms/reservation.html", {"av_room": av_room})


def calender_fields(request):

    times_inframe = []
    booked = Booked.objects.values("customer_details_id", "room_id")
    print(booked)
    for book in booked:
        title = Room.objects.filter(id=book["room_id"]).values("room_name")
        # print(title)
        user = Customer.objects.filter(id=book["customer_details_id"]).values("user")
        start_time = Customer.objects.filter(id=book['customer_details_id']).datetimes(
            "check_in", "minute", tzinfo=None)
        end_time = Customer.objects.filter(id=book['customer_details_id']).datetimes(
            "check_out", "minute", tzinfo=None)
        color = ["#"+''.join([random.choice('ABCDEF0123456789')
                             for i in range(6)])]
        times_inframe.append({"title": title[0]["room_name"],"user":user[0], "start": start_time[0].strftime(
            "%Y-%m-%d"), "end": end_time[0].strftime("%Y-%m-%d"), "color": color})

    return JsonResponse(times_inframe, safe=False, status=HTTPStatus.CREATED)



def update_user_dates(request):
    if request.method != "POST":
        return  HttpResponse("Method Not allowed")
    else:
        user = request.POST.get('user')
        start_date = request.POST.get("start_date")
        print(start_date)
        end_date = request.POST.get("end_date")
        # print(user,start_date,end_date)
        
        obj = get_object_or_404(Customer,user=user)
        obj.check_in = start_date
        obj.check_out = end_date
        obj.save() 

        return JsonResponse({"data":"Data updated sucessfully "},safe=False, status=HTTPStatus.CREATED)



def order(request):
    order = Order.objects.all()
    print(order)
    form = OrderCreationForm
    if request.method == "POST":
        form = OrderCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("order")
    context ={"form":form,"order":order}

    return render(request,"order.html",context)


def order_update(request,pk):
    order = get_object_or_404(Order,id=pk)
    form = OrderCreationForm(instance=order)
    if request.method == 'POST':
        form = OrderCreationForm(request.POST,instance=order)
        if form.is_valid():
            form.save()
        return redirect('order')
    context = {'form':form}
    return render (request,"order_update.html",context)

def order_delete(request,pk):
    customer = get_object_or_404(Order,id=pk)
    customer.delete()
    return redirect('order')

def customer(request):
    query = Customer.objects.all()[:6]
    context  = {"customer":query}
    return render(request,"customer.html",context)


def customer_update(request,pk):
    customer = get_object_or_404(Customer,id=pk)
    form = CustomerCretionForm(instance=customer)
    if request.method == 'POST':
        form = CustomerCretionForm(request.POST,instance=customer)
        print(request.POST)
        if form.is_valid():
            form.save()
        return redirect('customer')
    context = {'form':form}
    return render (request,"customer_update.html",context)


def delete_customer(request,pk):
    customer = get_object_or_404(Customer,id=pk)
    customer.delete()
    return redirect('customer')


def Kitchen_item(request):
    items = Kitchen_items.objects.all()
    form = KitchenItemCreationForm
    if request.method == "POST":
        form = KitchenItemCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("Kitchen_items")
    context ={"form":form,"order":items}

    return render(request,"kitechnitems.html",context)


def Kitchen_item_update(request,pk):
    items = get_object_or_404(Kitchen_items,id=pk)
    form = KitchenItemCreationForm(instance=items)
    if request.method == 'POST':
        form = KitchenItemCreationForm(request.POST,instance=items)
        if form.is_valid():
            form.save()
        return redirect('Kitchen_items')
    context = {'form':form}
    return render (request,"Kitchenitemsupdate.html",context)


def delete_kitchen_items(request,pk):
    items = get_object_or_404(Kitchen_items,id=pk)
    items.delete()
    return redirect('Kitchen_items')





def Bar_item(request):
    items = Bar_items.objects.all()
    form = BarItemCreationForm
    if request.method == "POST":
        form = BarItemCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("Kitchen_items")
    context ={"form":form,"order":items}

    return render(request,"baritems.html",context)


def bar_item_update(request,pk):
    items = get_object_or_404(Bar_items,id=pk)
    form = BarItemCreationForm(instance=items)
    if request.method == 'POST':
        form = BarItemCreationForm(request.POST,instance=items)
        if form.is_valid():
            form.save()
        return redirect('Kitchen_items')
    context = {'form':form}
    return render (request,"baritemsupdate.html",context)


def delete_bar_items(request,pk):
    items = get_object_or_404(Bar_items,id=pk)
    items.delete()
    return redirect('Kitchen_items')


def menu_items(request):
    return render(request,"menuitems.html")