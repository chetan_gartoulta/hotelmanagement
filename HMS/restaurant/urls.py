from django.urls import path
from .views import *
urlpatterns = [
    path("api_endpoint_overview/",api_endpoint_overview.as_view(),name="api_endpoint_overview"),


    path("create_list_customer/",create_list_customer.as_view()),
    path("customer_update_delete/<int:id>/",customer_update_delete.as_view(),name="customer_update_delete"),
    path("Reservation_booking/",Reservation_booking.as_view(),name="Reservation_booking"),
    path("Reservation_booking_update_delete/<str:id>/",Reservation_booking_update_delete.as_view()),

    # path("crud_customer/" ,crud_customer,name="crud_customer"),

    # path("crud_room/",crud_room,name="crud_room"),


    # path("booking_reservation/",booking_reservation,name="booking_reservation"),
    # # path("booking_reservation_room/<int:room_id>/",booking_reservation_room,name="booking_reservation_room"),

    

    # path("crud_categories/",crud_categories,name="crud_categories"),
    # path("crud_kitchen_items/",crud_kitchen_items,name="crud_kitchen_items"),
    # path("crud_bar_items/",crud_bar_items,name="crud_bar_items"),
    path("customer_order/",customer_order,name="customer_order"),

]
